# Planning your Internship Project

On any project, you first want to understand the requirements.
The requirements for a Tangible AI or Pro AI internship project is to deliver a written report on your project.
You will present that report to the other interns at the end of the 10 week internship.
Then you will add it to your portfolio on GitLab (and GitHub, if you insist).

So lay out the weeks ahead of you with that goal in mind.
You probably also want to have dates next to each week so you don't loose track of time:

## Example Plan for 2021 Spring Interns

1. 2021-03-27 brainstorm with mentors and peers about project ideas
2. 2021-04-03 select and refine a project idea, and give it a name
3. 2021-04-10 create a repository on gitlab, brainstorm on features or week-long tasks
4. 2021-04-17
5. 2021-04-24
6. 2021-05-01
7. 2021-05-08
8. 2021-05-15
9. 2021-05-22
10. 2021-05-29 prepare final report (due before 06-12)

## Planning

Once you have an idea for a project and a goal in mind (week 2), you can work forward from week 2 to week 9.
In week 2 you should work with your mentor to try to select and commit to a project.
Go ahead and give it a name, so you can easily mention it to your mentor and the other interns.
If you want to get ahead during week 2 you can use that name to create a GitLab repository for your project.
Or, if you'd prefer you can fork the `tangibleai/team` repository and put your project in a subfolder.

Start with the simpler foundational tasks.
Make a list of some of the features (tasks, or user stories) for your project.

Week 2 and 3 we will brainstorm some "features" you would like to create and start entering them into a repository (project) on gitlab.com.

Week 3 and 4 you will start implementing the most basic features, such as  an empty Django app with a working admin interface that you can run on your local machine with python manage.py runserver

That leaves week 5, 6, 7, and 8 (4 weeks) to add 4 significant  features (capabilities) to your app or project.
