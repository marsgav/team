
# Python Programming

* [ ] importing python packages: `this`, `os`, `sys`, `sys.path_append()`
* [ ] finding, installing, importing python packages: `sklearn`, `Pandas`, `tqdm`
* [ ] reading Tracebacks and error messages
* [ ] conditional expressions: `if`, `else`, `>`, `<`, `==`, `!=`, `not`, `in` 
* [ ] loops: `for`, `while`, `enumerate`, `zip`, `zip*`, `tqdm`
* [ ] functions: args, kwargs, `def`, `return`
* [ ] classes: `class`, methods, attributes, `__init__`, "everything is an object"
* [ ] scalar numerical data types: `int`, `float`
* [ ] sequence data types: `str`, `bytes`, `list`, `tuple`
* [ ] mapping: `dict`, `Counter`
* [ ] type coercion: `list(str)`, `dict([(1,2)]`, `dict(zip(range(4), 'abcd'))`
* [ ] sets: `set`
* [ ] getting help: `help()`, `?`, `??`, `[TAB] completion`, `vars()`, `dir()`, `type()`, `print()`, `.__doc__`)
* [ ] files: `with`, `open`, `write` 
* [ ] manipulate files with bash: `!`, `pwd`, `mkdir ~/code`, `mv`, `cp`, `touch`
* [ ] navigate directories: `cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`

These are called doctests. The question is the python code the line prefixed with `>>>`. The correct answer is the value you would put on the line after the python code based on what that expression would output.

```python
>>> dict(enumerate('Hello'))[1]
'e'
```

```python
>>> dict(enumerate('Hello'))[:-1]
TypeError: unhashable type: 'slice'
```

```python
>>> set('Oh Hello'.lower()) - set('Ohio ')
{'e', 'l'}
```

```python
>>> dict(Counter(list('ABBA'))
Counter({'A': 2, 'B': 2})
```

```python
>>> dict(zip(*['RD', '2'*2]))
{'R': '2', 'D': '2'}
```

```python
>>> pwd
>>> set(Path('.').absolute().__str__()) - set(_)
set()
```

```python
>>> def f(x):
...     while x > 2:
...         x -= 2
...     return x
>>> f(128)
2
```

```python
>>> 'lower' in dir('ect')
True
```

```python
>>> 'dir' in list('direct')
False
```

```python
>>> [1, 2, 3] * 2
[1, 2, 3, 1, 2, 3]
```

```python
>>> np.array([1, 2, 3]) * 2
[2, 4, 6]
```

```python
>>> pd.Series([1, 2, 3]) - 1
0    0
1    1
2    2
dtype: int64
```

```python
>>> pd.Series([1, 2, 3]) + pd.Series([1, 2, 3], index=[1, 2, 3])
0    NaN
1    3.0
2    5.0
3    NaN
```

