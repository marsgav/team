# Conversation UX Design

Maria Dyshel (CEO, Tangible AI) put together some elegant [graphics of the conversation design process](https://docs.google.com/document/d/1anmgpugcApoeRO36ed1Eznq0V8ZgxwoZkb5vkLp45Eo/edit?usp=sharing) and a video explaining conversation design principles (ask her where you can find it).

*Conversation UX design* is like UX design for most other apps.
You must understand your users' and stakeholders' needs and coalesce those needs into a feasible design:

1. interview stakeholders
2. interview users
3. develop a user persona (with stakeholders)
4. select and design features (with engineers)
5. requirements development (with engineers)

## Introduction

### Grice's Conversation Maxims (for humans)[^5]

When you're trying to help your user it's usually a good idea to be cooperative in the same way that humans attempt to be:

* **Quantity** Say no more or less than the conversation requires.
* **Quality**: Don't say what you believe to be false or for which you lack evidence.
* **Manner**: Don't be obscure, ambiguous. Be brief, orderly.
* **Relevance**: Say only relevant things.

You probably know a lot about these principles.
We learn them early on in life.
The challenge is applying these principles to a chatbot.
A chatbot cannot understand and compose natural language statements with as much subtlety and precision as you.

### Chatbot Design

As a chatbot conversation designer you have different limitations to deal with than in your everyday human conversations.

The challenging (and powerful) aspect of conversation UX design is that the UI is limited to a voice or chat interface.
You are designing a *conversational agent*, a *dialog system*, or a *chatbot*.
These all mean the same thing.
Your designing a computer program to engage in conversational dialog with your user.

For many chatbot platforms and interfaces, you will not get to customize the buttons and other visual cues.
Conventional graphical design affordances may not be an option for you.
Your main UX tool is usually the natural language text (often English) that you compose for the chatbot to *utter* to your users.
You also get to directly enter all the things you anticipate your users might say to the bot.

This is a powerful paradigm shift in app development.
It means you can implement your ideas directly into the *dialog system* without writing a line of code.
You only need to change the content (the words) within a text file to immediately see your ideas come to life in a chatbot.
This gives whole new meaning to the idea of rapid prototyping and Agile development.

Composing a chatbot is now as easy as composing the HTML for a website using a drag-and-drop GUI like Wix or a WSIWYG editor like Wordpress.
In fact there's an open source chatbot platform and framework called *Botpress* that imitates this Wordpress approach for chatbots.
More on that (and *Rasa*) later.

Though a conversation engine's capabilities are only limited by the content you can write in the data files, your user's requests are also limitless.
But user testing can usually uncover edge cases that you were unable to imagine during the design phase.
However, this explosive growth in complexity with every turn of the dialog means that you will quickly graduate from the GUI.
You will find it's much easier to maintain a large, complex chatbot if you take advantage of the tools programmers use to manage large software projects.
Text files in a structured hierarchy of folders can help.
Version control systems are another big improvement over what's possible within a GUI.
And code/dialog review within a git GUI such as GitLab.com can also be a huge boost in productivity and the quality of the bots you design.

Designing the UX of a conversational agent or dialog system is also sometimes referred to as voice-first design.
This is because the interface to the chatbot is often a speech recognition system and a voice synthesis system.
But you'll first want to understand the capabilities and limitations of a chatbot before you dig into the complexities of voice interface design.

## Chatbot Structure Design

*Conversation structure design* or *conversation engineering* is the crafting of content within a data structure that can be executed by a machine.
This data structure is then "run" like a python script, to enable the machine to have a conversation with a human.
In the early days, only software engineers were able to create dialog engines using programming languages like C and Python.
In the 20th century, however, data structures like AIML and other chatbot languages were invented that allowed even nonprogrammers to build sophisticated chatbots.

In order to understand all the ways users' needs might be satisfied with a chatbot you must first understand what a *dialog system* can and cannot do.
This is what you will do here as you learn how to design the structure of a conversation.
And this will require a bit of understanding about *Natural Language Processing* (NLP).

### Guard Rails

As you learned earlier, the things humans might say is limitless.
So rules-based bots have must put guard rails on the conversation.
It can help to provide UX affordances and "guard rails" or "guide rails" to nudge the user back into the parts of the dialog tree.
Guard rails keep the conversation flowing within the thick middle of the dialog tree.
This is where your bot is smartest.
This is where it has something interesting to say.

Some ideas for things for your chatbot can say when your user wanders too far from the "trunk" of your dialog tree:

- "I don't know. I get my information from wikipedia, and I can't find much about your question."
- "I don't understand. Could you say that a different way?"
- "Tell me more."
- "That's out of my wheelhouse. I'm really curious about you. Where did you hear about that?"
- "I can't understand you but I like you. Tell me more about you."
- "I'm not very smart yet. I can only talk about things like ..."

## NLU and NLG

A chatbot needs to be a good reader and a good texter. 

## NLU 

Listening is the most important part of being a good communicator. The same guess for your Chatbot. Even the most basic text generation approach can work well if paired with good natural language understanding algorithms. Because you can use template strings (Python f-strings) and fixed strings for text generation, you may not need text generation capability at all for your Chatbot. But if your Chatbot cannot read and understand text with NLU, it can't even get started.  

NLU can be used for two key tasks:

1. intent recognition
2. information extraction

### Intent Recognition

The word intent comes from the philosophical term intentionality. Intentionality refers to the meaning of words, their "aboutness", as philosophers like Daniel Dennet call it.[^13]  this aboutness is captured in embeddings. Embeddings are high dimensional tensors, usually vectors, that represent the relative amount of each component of meaning or aboutness. These embedding vectors are sometimes called *word vectors* when they describe the meaning of a single word. They are called docvectors when they describe the meaning in a phrase, sentence, paragraph, or other longer sequence of words. However you might prefer to use the term *thought vector*. A concept, or thought can sometimes be captured by a single word  and other times a sentence or more is required to capture and convey the thought. And when you're doing math on these vectors, you want to airfare that each dimension of the vector residents some particular aspect of the word's meaning. You can even transform or rotate these thought vectors to measure these components in ways that you care about, like the happiness in a thought, or the concreteness of the thought. There are an unlimited number of "ness" dimensions you might be interested in. It's like a character sheet in dungeons and dragons. Strength, agility, intelligence, speed, these aren't the only things you care about in a person, so you use words to describe them in more detail within their description. But you can't compare characters to each other, or do battle with dice and statistics, until you've parameterized your character with a list of numbers. You get to decide what goes into your nessvectors for words. You get to decide whether happiness or concreteness comes first in your list of numbers.

But in the end your Chatbot doesn't really care about the general "aboutness" of words. It just needs to make sure it understands what your particular user means or intends. For that you may want to use an NLU algorithm designed to understand whole sentences, or even paragraphs. Transformers are now the most effective way to process text and extract it's aboutness. Until recently LSTMs (including GRUs) were the state of the art for processing and embedding long sequences of natural unstructured symbols, such as DNA.

### Intent Recognition Approaches

1. string matching (exact, case-folded, normalized, edit distance)
2. keyword matching (exact, case-folded)
3. search (TFIDF, full text search, regex)
4. word vectors (GloVe, LDiA)
5. doc vectors (doc2vec, LSA)
6. CNNs
7. RNNs (LSTM, GRU)
8. Transformers (GPT, BERT, BigBird)

Most chatbot frameworks will allow you to select from among the first 3 basic NLU approaches. More advanced frameworks will incorporate the more advanced NLU algorithms to help improve your chatbots ability to listen well.

#### Context

Your user's intent has context, the history of all the things they've said up until now and everything your bot knows about the world. This can give you a clue about their intent.

### Information Extraction

Information extraction for a chatbot is very similar to information retrieval (search). Unlike information retrieval, you don't have to search a large corpus of text for a piece of information. You only need to search the text from your user. And you can normally do information extraction incrementally, one of sentence at a time. 

A typical application might be that you want to extract the users name from their introduction. If the user says "call me Ishmael", it's not enough to recognize that the user wants you to say something.  You'll probably  want an information extraction algorithm to retrieve that name "Ishmael" and store it in memory for use later, just as a human would. If your Chatbot is really smart it can even search the list of current participants in the conversation channel to make sure there's not an Ishmael that the "me" person was addressing. 

rmto and understand. Even simple text generation 
Most chatbot frameworks provide




## References

### Design Approaches

[^1]:Introduction to Conversation Design [video](https://www.youtube.com/watch?v=49G58PQWO7w) by Hans Van Dam, Co-Founder Robocopy
[^2]: "When to Humanize your Bots" [slides](https://docs.google.com/presentation/d/1rS5l29TWauxVEsuPhbqOFejr_2nAwcj2-A3JXYPTdKM/edit#slide=id.g58408d5e69_0_0) by chatbotcopywriter.com on Google Docs:
[^3]: "Perfecting the Chatbot Fallback Experience]" [article](https://uxplanet.org/perfecting-the-chatbot-fallback-experience-f76d119c45d4 ) by Casey Philips on Medium.com
[^4]: "Usability Heuristics for Bots" [article](https://thekevinscott.com/usability-heuristics-for-bots/) by Kevin Scott on his website
[^5]: "The Cooperative Principle in Conversation" [article](https://www.thoughtco.com/cooperative-principle-conversation-1689928) by Richard Nordquist, 2019 on ThoughtCo.com
[^6]: "Intro to Conversation Design" [slides](https://docs.google.com/document/d/1anmgpugcApoeRO36ed1Eznq0V8ZgxwoZkb5vkLp45Eo/edit?usp=sharing) and _video_ by Maria Dyshel, CEO, Tangible AI

### Technical Tools

[^7]: [pdf](https://web.stanford.edu/~jurafsky/slp3/24.pdf) _Stanford NLP_ book Ch 24, by Jurafsky (Stanford)
[^8]: [python](https://gitlab.com/tangibleai/qary/-/blob/master/src/qary/skills/quiz.py)  by Jose Robins
[^9]: [video](https://tan.sfo2.digitaloceanspaces.com/videos/howto/howto-build-your-first-rasa-chatbot-hobson-eda-50min-2021-02-15.mp4) "Your first Rasa chatbot"
[^10]: [tutorial](./your-first-rasa-chatbot.md) by Hobson Lane and Eda Firat
[^11]: [video](https://tan.sfo2.digitaloceanspaces.com/videos/howto/onboarding-howto-install-rasa-easy-nondeveloper-way-also-conda-create-environment-and-python-paths-using-bash-which-python-pip-tilde-by-hobs-eda-2020-12-31.mp4) "Install Rasa the easy way" by Hobson Lane and Eda Firat
[^12]:  [video](https://tan.sfo2.digitaloceanspaces.com/videos/howto/onboarding-how-to-install-qary-john-2020-11-20.mp4) "How to install qary" by John May
[^13]: Intuition pump #13 "Murder in Trafalgar Square" [Intuition Pumps and other Thinking Tools](proai.org/intuition-pumps) by Daniel Dennet. 


### Glossary

* **affordance**: A way to interact with the UX. Examples in the real world might be physical button, switch, knob, or a door handle. If designed well, an affordance needs no label or instructions.
* ** dialog tree**: A directed graph of connections between the states of a chatbot or dialog engine or conversation design. In programming lingo these are sometimes referred to as Finite State Machines or Finite State Transducers.
* ** domain specific language** (DSL): A language or data structure intended to be run or executed by a specialized software program called an interpreter or compiler. Some examples you may have heard of include Markdown, HTML, and YAML.
* ** intent**: A particular action or utterance that a human desires to invoke in the chatbot. In conversation engineering, a list of intents represents forks in a dialog tree.
* ** natural language**: Sequences of symbols (language) used by humans or other animals in their communication with each other. The word "natural" is intended to distinguish natural languages from programming languages for computers. Some examples of natural languages include English, Spanish, Amharic, Navaho, Chinese, and even whistling or bird song.
* ** natural language processing** (NLP): Computing some output value based on a natural language sequence. A chatbot or dialog system is one such processor for natural language intended to output or generate natural language that responds to a user's text messages. NLP is often divided into *natural language understanding* (NLU) and *natural language generation* (NLG).
* ** natural language understanding**: Computing the user's intent or sentiment based on the text of their message or utterance.
* ** natural language generation**: Computing a sequence of natural language words to convey some though or emotion to the recipient of the text message.
* ** phone tree**: An automated telephone answering service widely used as far back as the 80's and 90's. It works with land line telephone systems as well as mobile phones using the touch-tone system where users press numbers on their phone. Example: "Press 1 for English, 2 for Spanish, 0 for the main menu".
* ** state**: A mode of operation that a machine switches too with each utterance by the user. Within a particular state, the machine will only react to the list of messages or *intent*s from the user that were assigned to that *state* by the conversation designer.
* ** story**: Rasa term for a chain or tree of turns -- sequence of potential interactions between a bot and a human.
* ** turn**: A pair of chat messages or utterances from two parties in a conversation
* ** utterance**: A chat message from a bot or a human -- from the word "utter", which means to say
