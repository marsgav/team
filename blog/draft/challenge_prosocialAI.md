# intern-challenge prosocial ai

What does prosocial AI mean? 

Find futurists, technologists, researchers, and philosophers that are worried about the AI control problem or the technological singularity or the economic singularity. Terms like "loyal ai" vs "beneficial ai" and AGI might be good searches in a prosocial search engine like Duck.com or scholar.google.com. You will probably enjoy connecting to the low level, marginalized engineers who are getting down to work building open source packages that democratize AI and nudge it to be more prosocial. Try to network and follow at least 10 such people on GitLab, reddit, twitter, linked in, or github (in order from least antisocial business to more antisocial). And share your finds on Slack

## Humans to watch

The following are some prosocial ai researchers, teachers, engineers, sociologists, and influencers that stood out in the past few months:

- Paul W.B. Atkins: [Prosocial: Using Evolutionary Science to Build Productive, Equitable, and Collaborative Groups] by Paul W.B. Atkins PhD, 2019
- Dr Tamsin's [Teeming Superorganisms](https://www.amazon.com/Teeming-Superorganisms-Together-Infinite-company/dp/1940468426)
- Zeynep Tufekci
- Melanie Mitchel
- Kathryn Soo McCarthy
- Martin Nowak & Roger Highfield
- Stuart Russel (_AIMA_ and _Human Compatible_)
- Jake Porway (founder DataKind, thought leader at data.org)
- Peter Norvig?
- Helen Pluckrose & Peter Boghossian
- Rob Reid
- some of Lex Fridman's guests
- Silva Micali (Turing Award winner, Algorand cryptocurrency inventor)
- Marcus Hutter
- Susan Schneider (_Artificial You_)
- Joy Buolamwini (Coded Bias on Netflix)
- [Julie Pagano](https://juliepagano.com/blog/2014/05/10/so-you-want-to-be-an-ally/)

Try to find related, but less famous people, so that you get the most prosocial leverage for you likes.

## Organizations (algorithms) to Watch

And here some prosocial organizations that might be good for your prosocial search:

- Omdena
- [DataKind](https://www.datakind.org/) - "algorithms ... companies use to boost profits can be leveraged by mission-driven organizations to improve the world"
- [data.org](data.org) - "partnerships to build the field of data science for social impact"
- [Dimagi](https://www.dimagi.com/) - "technology for social-impact" - makers of [CommCare](https://github.com/dimagi/commcare-core)
- Plan International
- Open Science Foundation
- fsf.org
- eff.org
- osf.io

## Books and Articles to check out

Here are some books relating to prosocial AI

- Atlas of AI by Kate Crawford
- [Model Interpretability](https://dl.acm.org/doi/10.1145/3236386.3241340)
