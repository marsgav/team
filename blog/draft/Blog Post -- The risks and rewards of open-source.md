# Risks and rewards of open-source?

Maybe you're a startup seeking funding from investors that value "IP".
Maybe you have an idea for a cool new killer app to save the world.
Should open source software be a part of your business plan?

You're worried about competitors using our software to outcompete you in the marketplace.
You're worried about investors asking you to "build a moat" around your business.

I hear you.
I've been there ... literally.

I've been a data scientist, CTO, and cofounder at more than 7 startups in the past 15 years.
Most started out with siginicant funding.
Some hired a great CTO (me) to help steer the ship.
Some were spinoffs backed by the likes of Intel and Sharp Labs.
All but one of those have since disolved or stagnated.
None have gone public or receive 10s of millions in funding.
It's not that interesting that I've been unlucky with the imbalanced dice that determine a startup's fate.[^1]

What is interesting is the post mortem.
In fact I can almost do a premortem on any company I talk to now.
I really only need to ask one question of the founders.

> Are you 100% focused on creating a product that maximizes value for your customers?

Of course almost all founders will say "Yes! Absolutely!"
So the real question is, can they prove it to you.
And there's a good way to find out.
So the real question to ask them (or yourself) is

> If you discovered something free, something that would improve the customer experience, your operations efficiency, adoption rate, security, and trustworthiness of your product, would you do it?

Again, most founders will still say yes.
No founder will ever leave money on the table.
But if they don't ask you for an example, then they've failed your test.
If they do ask for an example, that's your chance to dig deeper into their product and see if maybe open source could give their product and their business a boost.

> Do you think open source would improve your customer value?

Most people agree that open source definitely improves the efficiency of an operation.
No more NDAs, no more lawyers, no more patents and patent trolls harassing your business.
And no more private repos without vital productivity-enhancing features like CI-CD pipelines or issue trackers or project management tools.
You may not be aware, though, that it will also improve the efficiency and efficacy of your recruiting.
Your most valuable employees will likely come from a vetted pool of contributors to your open source project.
And developers will flock to you even when all you can offer them is some equity and a brilliant idea.
A battle hardened developer knows that if he works for a closed-source startup that goes bust, he (and the early customers) walk away empty handed.

In all but one of the startups in my past, I failed to convince the founders to open source their core technology.
The only one that is still growing exponentially is the one fanatically dedicated to open source -- [Tangible AI](https://tangibleai.com).
It's still early yet (19 months in), but I like our chances.

Here are some other recent success stories for open source along with their current age and funding.
* [Open AI](https://www.crunchbase.com/organization/openai): 6 yr, $1B
* [Hugging Face](https://techcrunch.com/2020/06/23/rasa-raises-26m-led-by-a16z-for-its-open-source-conversational-ai-platform/): 5 yr, $60M
* [Rasa](https://www.crunchbase.com/organization/rasa): 5 yr, $40M
* [Moodle](https://www.crunchbase.com/organization/moodle): 19 yr, $6M

More interested in the Unicorns? Though most of these serve developers, so open source is clearly the right choice, they are still worth mentioning (it's know wonder your developers love to work for open source companies).
And as a data scientist doing linear regression in my head on this data, I can't help but notice the correlation between openness and growth rate.[^2] Gitlab even open sources their business process documentation and product development meeting minutes.

* [Gitlab](https://gitlab.com): 7 yr, $434M
* [Docker](https://docker.com): 8 yr, $330M
* [Digital Ocean](https://digitalocean.com): 9 yr, $450M
* [Aiven](https://siliconangle.com/2021/03/23/managed-open-source-software-startup-aiven-raises-100m-series-c-funding/): 5 yr, $150M
* [Whitesource](https://www.crunchbase.com/organization/white-source): 10 yr, $120M
* [Redhat, Redis, Databricks, Camuda](https://adevait.com/blog/future-of-work/10-best-open-source-startups)
* Mysql, Postgres, Neo4J, Starburst
* Puppet, Chef, Ansible
* Ubuntu
* I could go on and on...

## Execution, execution, execution

For retail, it's location. For software, it's execution. No body cares about your idea, not your investors, not your customers. All they care is can you make their lives better. And if you can'd execute quickly and efficiently on your idea, it's doesn't matter to anyone (except your competitors who will run with it and succeed where you failed).

And as any manager knows communication is key when it comes to building a performant team that builds killer apps.
You users become contributors to your feature selection and quality control processes.
And their feedback matters so much more than what your UX designer's user interviews and focus groups say.
Your users are using your product in the real world.
You can't get better feedback than organic, grass roots feedback.
Your main struggle with your developers is just getting them to ignore their own personal use cases, so they will build a product for your users instead of for themselves.

For a closed source company, any time or energy your team spends protecting your idea is wasted the moment someone outside of your organization comes up with a better idea or approach or feature.
And your relationship with your users is upside down.
Your users' ideas are a threat to your success in a closed source company.
They are more likely to share it with their developer friends than to share it with the company that is charging them for a product that they just thought up a way to improve.

If you decide you want to have a monopoly, a closed-source foundation for your company, you will have to be the very best at serving your customers.
That's hard when you and your teammates are all alone in a vast sea of ideas and tech swirling around you.
It's much easier to be the best when you harvest the wisdom and ideas from your users with an open source foundation.
Later, once you are a roaring success, you can start thinking of ways to upsell or monetize your users.
There's very little risk to starting out as open source.
You can always button things up on those features you think are going to be most profitable.

But my hope is, you will never want to turn back.
Once you've experienced the power of an open source community behind you, you will never see business the same way again.

### Footnotes

[^1]: https://review42.com/resources/what-percentage-of-startups-fail/ "90% of startups fail"
[^2]: https://www.crunchbase.com/discover/organization.companies/80aa15a6fd10845452fff1c9419a07c8 "*open source* on Crunchbase"
