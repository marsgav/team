# Natural Language Feature Engineering

In order to be able to make predictions, we need to do math.
If your data is a natural language string, such as the following quote from _Artificial You_ by Susan Schneider

```python
>>> s = """
...     Michael Bess has called this the Jetsons Fallacy.
...     In reality, AI will not just transform the world.
...     It will transform us.
...     -- Susan Schneider
...     -- _Artificial You_
...     """
```

What are some statistics or numbers you could associate with a string of characters?

- `len(s)`: number of characters
- `len(s.split())`: approximate number of words
- `len(set(s))`: number of unique characters
- `len(set(s.split()))`: number of unqiue words
- `len(s.split('\n'))`

So with these simple one-liners we could generate a vector of 4 integers.
That would be more than enough to adequately solve (80%+ accuracy) many simple classification problems:

- full name or given name
- poem or title
- sentence or paragraph

What about some harder questions:

- important message or spam?
- text message from professor, boss, or friend?
- profound or silly?
- from a book, poem, blog post, or news article?
- from a nonfiction or fiction book?

For those we'll need more complex numerical representations of words.
Four dimensions is not enough.
So lets think about some statistics that we can generate automatically that might capture more subtle details.

For example, rather than counting up all the letters in a string, you can count up each individual letter separately.
The built in `collections.Counter` class makes this a one-liner.
The `Counter` class returns a dictionary.
Each unique element of the input sequence, a character in this case, becomes a key.
And the value associated with that character is the inter count of the number of times it occurs in that sequence.


```python
>>> from collections import Counter
>>> letter_counts = Counter(s)
>>> letter_counts['M']
1
>>> letter_counts['.']
3
```



Can you think of anything simpler than len()
