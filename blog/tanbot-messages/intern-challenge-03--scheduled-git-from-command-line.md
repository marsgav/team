# Week 3: Git from the Command Line

#### _`date_scheduled = datetime(2021, 2, 3, 30, 0)`_

#### _`title = 'scheduled'`_

For Week 3 you are going to learn how to use git from the command line.
You've already tried out the GUI at gitlab.com, but now you're going to learn how the professionals do it.
This will build on the shell commands that you already know (`cd`, `ls`, `rm`, `mkdir`, `nano`, and `more`).
If these commands aren't familiar to you yet, perhaps this video by Eda Firat will help: []()  
Now that you've had to endure all these naggy messages for 9 weeks, I thought you might like to see how Tanbot works.
The assignment this week is to try your hand at giving Tanbot something to say.

Here are the keys to the Tanbot kingdom:

admin interface: http://staging.qary.ai/admin/
username: Tanbot
password: tangible1

Once you're logged in you can see all the tables of data that Tanbot uses to do its work. Explore around and see if you can figure out what each one is for.  And here's your mission, should you chose to accept it: [Give Tanbot Something Interesting to say](gitlab.com/tangibleai/team/-/blob/master/src/team/interns/2021_winter/intern-challenges/intern-challenge-09--give-tanbot-something-to-say.md) 

