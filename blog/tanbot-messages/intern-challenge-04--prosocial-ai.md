# intern-challenge-04 prosocial ai

While you're waiting for this week's lesson on word vectors (word embeddings), you can get started on the bonus assignment... researching and supporting prosocial ai.

Find futurists, technologists, researchers, and philosophers that are worried about the AI control problem or the technological singularity or the economic singularity. Terms like "loyal ai" vs "beneficial ai" and AGI might be good searches in a prosocial search engine like Duck.com or scholar.google.com. You will probably enjoy connecting to the low level, marginalized engineers who are getting down to work building open source packages that democratize AI and nudge it to be more prosocial. Try to follow at least 10 such people on GitLab, reddit, twitter, linked in, or github (in order from least antisocial business to more antisocial). And share your finds on Slack

## Humans to watch

The following are some prosocial ai researchers, teachers, engineers, sociologists, and influencers that stood out in the past few months:

- Paul W.B. Atkins: [Prosocial: Using Evolutionary Science to Build Productive, Equitable, and Collaborative Groups] by Paul W.B. Atkins PhD, 2019
- Dr Tamsin's [Teeming Superorganisms](https://www.amazon.com/Teeming-Superorganisms-Together-Infinite-company/dp/1940468426) and Superorganisms
- Carol Sanford's _Regenerative Businesses_
- Isabel Wen's "San Diego Next" organization supporting regenerative businesses promoting the idea that wealth accumulation is a symptom of antisocial, unethical behavior and values
- Zeynep Tufekci
- Melanie Mitchel
- Kathryn Soo McCarthy
- Martin Nowak & Roger Highfield
- Stuart Russel (_AIMA_ and _Human Compatible_)
- Peter Norvig
- Helen Pluckrose & Peter Boghossian
- Rob Reid
- some of Lex Fridman's guests
- Silva Micali (Turing Award winner, Algorand cryptocurrency inventor)
- Marcus Hutter
- Daniel Dennet (_Intuition Pumps_ and _Theory of Consciousness_)[^1]
- Susan Schneider (_Artificial You_ )[^2]
- Joy Buolamwini (Coded Bias on Netflix)
- Isabel Wu

Try to find related, but less famous people, so that you get the most prosocial leverage for you likes.

## Organizations (algorithms) to Watch


### Antisocial Orgs

Nonprofits, schools, and orgs that loudly claim to be mission-driven are not always prosocial. The mission" is often self-serving, to create wealth, political influence, and power for the executives in charge. In fact, the largest, most popular nonprofits are virtual pawns of the US State Department or Corporations that benefit monetarily from their "charity."

- Red Cross
- Good Will
- Omdena
- Hillsdale College (think tank)
- Many newsletters (disguised propaganda)
- Citizens United
- Cambridge Analytica
- Big-tech funded charities that donate services and "store" credit to those in need
- Many edtech organizations

### Prosocial Orgs

And both for-profit and nonprofit organizations are very prosocial. 
Here some prosocial organizations that might be good for your prosocial search. 
Also, you can help these companies with SEO by searching for them in [Duck.com](DuckDuckGo.com) (or google, if you must). Finding them among the other distractor ads and click on the one for the domain name listed below.

- Insight Out (insightout.community)
- Free Geek (freegeek.org)
- Data Kind (datakind.org)
- Dimagi (dimagi.com)[^coi]
- ONOW (onow.org)[^coi]
- Plan International (plan-international.org)[^coi]
- Bottom Line (bottomline.org/)[^coi]
- Open Science Foundation (osf.io)
- Free Software Foundation (fsf.org)
- Electronic Frontier Foundation (eff.org)

### Footnotes

[^1]: Daniel Dennet's _Theory of Consciousness_ [summary](https://schneiderwebsite.com/uploads/8/3/7/5/83756330/daniel_dennetts_theory_of_consciousness.pdf) by Susan Schneider
[^2]: Susan Schneider's _Artificial You_ [Ch 12 "Alien Consciousness"](https://schneiderwebsite.com/uploads/8/3/7/5/83756330/schneider_9781107109988c12_p189-206.pdf)
[^COI]: Conflict of Interest Disclosure: Bottom Line, Dimagi, ONOW, and Plan International are all Tangible ai Customers


