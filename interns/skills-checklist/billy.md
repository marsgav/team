# Skills Checklist

These are the skills that employers expect from Junior Coding Data Scientists, Machine Learning Engineers, or even Junior Software Developers.
Think of this as what to expect on your final exam.
You'll learn much more than this.
But these are the skills that I find most professional software development teams expect from a junior developer.
 to see if you have what it takes to contribute to a typical Python software development team.

* 01: Programming (Python):
    * [X] install packages from pypi
    * [X] importing modules within packages like sklearn and Pandas
    * [ ] install packages from source code (--editable)
    * [X] install and import packages from gitlab
    * [X] conditions: `if`, `else`
    * [X] loops: `for`, `while`
    * [X] functions (args, kwargs, `return`)
    * [ ] classes: `class`, methods, attributes
    * [X] scalar numerical data types (`int`, `float`)
    * [X] sequence data types (`str`, `bytes`, `list`, `tuple`)
    * [X] mapping (`dict`, `Counter`)
    * [X] sets (`set`)
    * [X] create stackoverflow account
    * [X] reading Tracebacks and error messages
    * [X] getting help (`help()`, `?`, `??`, `[TAB] completion`, `vars()`, `dir()`, `type()`, `print()`, `.__doc__`)
    *
* 02: Shell (Bash)
    * [ ] bash nlp (`wc`, `grep`, `sed` or `awk` [stanford.edu/class/cs124/lec/textprocessingboth.pdf])
    * [X] navigate directories (`cd`, `pwd`, `ls -hal`, `/`, `~`, `*`, `$HOME`)
    * [X] manipulate files + directories (`mkdir ~/code`, `mv`, `cp`, `touch`)
    * [X] work with text files (`more`, `cat`, `nano`)
    * [ ] pipes and redirects (`|`, `>`, `>>`, `<`)
    * [ ] processes (`ps aux`, `fg`, `bg`, `&`)
    * [ ] conditions (`&&`, `||`)
    * [X] tab-completion
    * [ ] comments and shabang (`# `, `#!`)
    * [ ] permissions (`chmod`, `chown`)
    * [ ] running shell commands (`source`, `.`, `eval`)
    * [ ] finding files: `find . -iname qary -size +1k`
    * [ ] ssh to remote server: `ssh intern@totalgood.org`
    * [X] set up ssh keys: `ssh-keygen`, `ssh-copy-id`
    *
* 03: Git
    * [X] create gitlab account
    * [X] find and fork a project on gitlab.com
    * [ ] create project in gitlab.com
    * [x] edit a file in gitlab.com (README.md)
    * [ ] add a file using gitlab.com GUI
    * [ ] upload a file to gitlab.com with GUI
    * [X] ssh public key in gitlab
    * [X] clone a repository from the command line
    * [ ] `git branch`
    * [ ] `git merge`
    * [ ] intentionally create and resolve merge conflicts
    * [ ] habitually use the `status`, `add`, `commit -am`, `push`
    *
* 04: Data
    * [X] loops: `enumerate`, `tqdm`
    * [X] list comprehensions to transform features: `[x**2 for x in array]`
    * [X] conditional list comprehension: `[x**2 for x in array if x > 5]`
    * [X] load csv: `df = pd.read_csv()`,  `df = pd.read_csv(sep='\t')`
    * [ ] DataFrames from HTML tables: `df = pd.read_html()`
    * [ ] bigdata: `df = pd.read_csv(chunk_size=...)`
    * [X] vectorized operations: `x1 + x2`
    * [X] concatenate: `pd.concat(axis=0/1)`
    *
* 05: Statistics & Data
    * [X] `np.random.rand`, `.randint`, `.randn`, `np.random.seed`
    * [X] standard deviation
    * [ ] error metrics (RMSE, loss)
    * [ ] optimization (objective function, gradient descent)
    * [ ] [68-95-99.7 Rule](https://en.wikipedia.org/wiki/68%E2%80%9395%E2%80%9399.7_rule)
    * [X] histograms
    * [ ] polynomial features
    * [X] linear regression
    * [X] logistic regression
    *
* 06: Basic AI understanding
    * [X] make predictions/estimates with pretrained machine learning model
    * [X] training/fitting a machine learning model on a training set
    * [X] validating/evaluating/testing a model on a validation set or test set
    * [X] overfitting: how to detect it, and what to do about it
    * [ ] class bias: how to detect it, and what to do about it
    *
* 07: Networking
    * [ ] python `requests` package and `.get`, `.post` methods and `stream=True`
    * [ ] `pandas.read_html()` function
    * [ ] python `BeautifulSoup4` package
    * [ ] address bar in browser for HTTP GET requests with arguments
    * [ ] address bar in browser for HTTPS requests with arguments
    * [ ] `wget` or c`url` for command line GET requests with arguments
    * [ ] `wget` or `curl` for command line POST requests with data (arguments)
    *
* 08: Best practices
    * [X] style guides
    * [X] PEP8
    * [ ] linters
    * [ ] Syntax highlighters
    * [ ] [Napolean-style docstrings](https://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html)
    * [ ] [doctests](https://django-testing-docs.readthedocs.io/en/latest/basic_doctests.html)
    * [ ] Anaconda auto-formatter for Sublime
    * [ ] zen of python
    * [ ] constructive [code reviews](https://youtu.be/iNG1a--SIlk)
    * [ ] patterns and [antipatterns](http://docs.quantifiedcode.com/python-anti-patterns/)
    * [ ] [testing](https://python-102.readthedocs.io/en/latest/testing.html) (`pytest`, TDD, `unittest`, regression tests)
    *
* 09: NLP and chatbots:
    * [ ] chatbot architectures (deterministic, generrative, knowledge-based)
    * [ ] intents and intent recognition
    * [ ] information extraction, entities
    * [ ] search
    * [ ] utterances
    * [ ] conversation management
    * [ ] voice recognition and multimodal UI (AllenAI)
    *
* 10: Business:
    * [ ] Agile (standups, retros, planning, poker)
    * [ ] Introduction to Tangible AI
    * [ ] "How nonprofits use AI" webinar
    * [ ] Open source principles
    * [ ] Open source licenses and how to choose the right one
    *
